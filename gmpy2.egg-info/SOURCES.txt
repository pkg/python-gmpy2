COPYING
COPYING.LESSER
INSTALL
MANIFEST.in
README.rst
pyproject.toml
setup.py
docs/Makefile
docs/advmpz.rst
docs/conf.py
docs/contexts.rst
docs/conversion.rst
docs/cython.rst
docs/exceptions.rst
docs/generic.rst
docs/history.rst
docs/index.rst
docs/install.rst
docs/make.bat
docs/misc.rst
docs/mpc.rst
docs/mpfr.rst
docs/mpq.rst
docs/mpz.rst
docs/overview.rst
docs/tutorial.rst
docs/win_build.rst
gmpy2/__init__.pxd
gmpy2/__init__.py
gmpy2/gmpy2.h
gmpy2/gmpy2.pxd
gmpy2.egg-info/PKG-INFO
gmpy2.egg-info/SOURCES.txt
gmpy2.egg-info/dependency_links.txt
gmpy2.egg-info/not-zip-safe
gmpy2.egg-info/requires.txt
gmpy2.egg-info/top_level.txt
src/gmpy2.c
src/gmpy2.h
src/gmpy2.pxd
src/gmpy2_abs.c
src/gmpy2_abs.h
src/gmpy2_add.c
src/gmpy2_add.h
src/gmpy2_binary.c
src/gmpy2_binary.h
src/gmpy2_cache.c
src/gmpy2_cache.h
src/gmpy2_cmp.c
src/gmpy2_cmp.h
src/gmpy2_const.c
src/gmpy2_const.h
src/gmpy2_context.c
src/gmpy2_context.h
src/gmpy2_convert.c
src/gmpy2_convert.h
src/gmpy2_convert_gmp.c
src/gmpy2_convert_gmp.h
src/gmpy2_convert_mpc.c
src/gmpy2_convert_mpc.h
src/gmpy2_convert_mpfr.c
src/gmpy2_convert_mpfr.h
src/gmpy2_convert_utils.c
src/gmpy2_convert_utils.h
src/gmpy2_divmod.c
src/gmpy2_divmod.h
src/gmpy2_floordiv.c
src/gmpy2_floordiv.h
src/gmpy2_format.c
src/gmpy2_format.h
src/gmpy2_fused.c
src/gmpy2_fused.h
src/gmpy2_hash.c
src/gmpy2_hash.h
src/gmpy2_macros.h
src/gmpy2_math.c
src/gmpy2_math.h
src/gmpy2_minus.c
src/gmpy2_minus.h
src/gmpy2_misc.c
src/gmpy2_misc.h
src/gmpy2_mod.c
src/gmpy2_mod.h
src/gmpy2_mpc.c
src/gmpy2_mpc.h
src/gmpy2_mpc_misc.c
src/gmpy2_mpc_misc.h
src/gmpy2_mpfr.c
src/gmpy2_mpfr.h
src/gmpy2_mpfr_misc.c
src/gmpy2_mpfr_misc.h
src/gmpy2_mpmath.c
src/gmpy2_mpq.c
src/gmpy2_mpq.h
src/gmpy2_mpq_misc.c
src/gmpy2_mpq_misc.h
src/gmpy2_mpz.c
src/gmpy2_mpz.h
src/gmpy2_mpz_bitops.c
src/gmpy2_mpz_bitops.h
src/gmpy2_mpz_divmod.c
src/gmpy2_mpz_divmod.h
src/gmpy2_mpz_divmod2exp.c
src/gmpy2_mpz_divmod2exp.h
src/gmpy2_mpz_misc.c
src/gmpy2_mpz_misc.h
src/gmpy2_mpz_pack.c
src/gmpy2_mpz_pack.h
src/gmpy2_mul.c
src/gmpy2_mul.h
src/gmpy2_muldiv_2exp.c
src/gmpy2_muldiv_2exp.h
src/gmpy2_plus.c
src/gmpy2_plus.h
src/gmpy2_pow.c
src/gmpy2_pow.h
src/gmpy2_predicate.c
src/gmpy2_predicate.h
src/gmpy2_random.c
src/gmpy2_random.h
src/gmpy2_richcompare.c
src/gmpy2_richcompare.h
src/gmpy2_sign.c
src/gmpy2_sign.h
src/gmpy2_square.c
src/gmpy2_square.h
src/gmpy2_sub.c
src/gmpy2_sub.h
src/gmpy2_truediv.c
src/gmpy2_truediv.h
src/gmpy2_types.h
src/gmpy2_vector.c
src/gmpy2_vector.h
src/gmpy2_xmpz.c
src/gmpy2_xmpz.h
src/gmpy2_xmpz_inplace.c
src/gmpy2_xmpz_inplace.h
src/gmpy2_xmpz_limbs.c
src/gmpy2_xmpz_limbs.h
src/gmpy2_xmpz_misc.c
src/gmpy2_xmpz_misc.h
src/gmpy_mpz_lucas.c
src/gmpy_mpz_lucas.h
src/gmpy_mpz_prp.c
src/gmpy_mpz_prp.h
src/mpz_pylong.c
src/pythoncapi_compat.h
test/conftest.py
test/supportclasses.py
test/test_consts.py
test/test_context.py
test/test_functions.py
test/test_generic.py
test/test_misc.py
test/test_mpc.py
test/test_mpfr.py
test/test_mpq.py
test/test_mpz.py
test/test_xmpz.py